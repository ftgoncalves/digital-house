package com.ftgoncalves.firebaseauthentication;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
	@Override
	public void onMessageReceived(RemoteMessage remoteMessage) {
		super.onMessageReceived(remoteMessage);

		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		Notification notification = new NotificationCompat.Builder(this, "default")
						.setSmallIcon(R.drawable.ic_android_black_24dp)
						.setContentTitle(remoteMessage.getData().get("title"))
						.setContentText(remoteMessage.getNotification().getBody())
						.setAutoCancel(true)
						.build();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel notificationChannel = new NotificationChannel(
							"default",
							"default",
							NotificationManager.IMPORTANCE_HIGH);

			notificationManager.createNotificationChannel(notificationChannel);
		}

		notificationManager.notify(1, notification);
	}
}
