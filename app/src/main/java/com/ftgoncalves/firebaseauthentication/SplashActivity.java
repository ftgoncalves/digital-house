package com.ftgoncalves.firebaseauthentication;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

public class SplashActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				if (!isUserAuthenticated()) {
					Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
					startActivity(intent);
				} else {
					Intent intent = new Intent(SplashActivity.this, MainActivity.class);
					startActivity(intent);
				}
				finish();
			}
		}, 2000);
	}

	private boolean isUserAuthenticated() {
		return FirebaseAuth.getInstance().getCurrentUser() != null;
	}
}
