package com.ftgoncalves.firebaseauthentication.paint;

public class Paint {
	public String artistId;
	public String image;
	public String name;

	public Paint() {
	}

	public Paint(String artistId, String image, String name) {
		this.artistId = artistId;
		this.image = image;
		this.name = name;
	}

	public String getArtistId() {
		return artistId;
	}

	public void setArtistId(String artistId) {
		this.artistId = artistId;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
