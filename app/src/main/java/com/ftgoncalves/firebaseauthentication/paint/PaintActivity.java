package com.ftgoncalves.firebaseauthentication.paint;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.ftgoncalves.firebaseauthentication.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaintActivity extends AppCompatActivity {

	@BindView(R.id.paints)
	RecyclerView paints;

	private final FirebaseDatabase database = FirebaseDatabase.getInstance();

	private PaintAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_paint);
		ButterKnife.bind(this);

		final String artistId = getIntent().getStringExtra("artistId");

		LinearLayoutManager layout = new LinearLayoutManager(this);
		paints.setLayoutManager(layout);
		adapter = new PaintAdapter(this);
		paints.setAdapter(adapter);

		DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
						layout.getOrientation());
		paints.addItemDecoration(dividerItemDecoration);

		DatabaseReference artistsRef = database.getReference("paints");

		artistsRef.addValueEventListener(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
				Iterable<DataSnapshot> snapshotIterable = dataSnapshot.getChildren();
				for (DataSnapshot snapshot : snapshotIterable) {

					Paint paint = snapshot.getValue(Paint.class);
					if (paint.artistId.equals(artistId)) {
						adapter.addPaint(paint);
					}
				}
			}

			@Override
			public void onCancelled(DatabaseError error) {
				Toast.makeText(PaintActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
			}
		});
	}
}
