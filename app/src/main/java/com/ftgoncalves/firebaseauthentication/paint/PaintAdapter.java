package com.ftgoncalves.firebaseauthentication.paint;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ftgoncalves.firebaseauthentication.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PaintAdapter extends RecyclerView.Adapter<PaintAdapter.PaintHolder> {

	private final Context context;

	private List<Paint> artists = new ArrayList<>();

	public PaintAdapter(Context context) {
		this.context = context;
	}

	public void addPaint(Paint paint) {
		this.artists.add(paint);
		notifyItemInserted(artists.size() - 1);
	}

	@NonNull
	@Override
	public PaintAdapter.PaintHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(context)
						.inflate(R.layout.paint_item, parent, false);

		return new PaintHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull PaintAdapter.PaintHolder holder, int position) {
		Paint paint = artists.get(position);

		Picasso.get()
						.load(paint.image)
						.into(holder.image);

		holder.name.setText(paint.getName());
	}

	@Override
	public int getItemCount() {
		return artists.size();
	}

	class PaintHolder extends RecyclerView.ViewHolder {

		ImageView image;
		TextView name;

		PaintHolder(View itemView) {
			super(itemView);

			name = itemView.findViewById(R.id.name);
			image = itemView.findViewById(R.id.image);
		}
	}
}
