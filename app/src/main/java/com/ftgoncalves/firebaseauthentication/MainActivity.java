package com.ftgoncalves.firebaseauthentication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.ftgoncalves.firebaseauthentication.artist.Artist;
import com.ftgoncalves.firebaseauthentication.artist.ArtistAdapter;
import com.ftgoncalves.firebaseauthentication.paint.PaintActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements ArtistAdapter.ArtistClickListener {

	@BindView(R.id.artists)
	RecyclerView artists;

	private final FirebaseDatabase database = FirebaseDatabase.getInstance();

	private ArtistAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);

		LinearLayoutManager layout = new LinearLayoutManager(this);
		artists.setLayoutManager(layout);
		adapter = new ArtistAdapter(this);
		artists.setAdapter(adapter);

		DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
						layout.getOrientation());
		artists.addItemDecoration(dividerItemDecoration);

		DatabaseReference artistsRef = database.getReference("artists");

		artistsRef.addValueEventListener(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
				Iterable<DataSnapshot> snapshotIterable = dataSnapshot.getChildren();
				for (DataSnapshot artist : snapshotIterable) {
					adapter.addArtist(artist.getValue(Artist.class));
				}
			}

			@Override
			public void onCancelled(DatabaseError error) {
				Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public void onArtistClick(String id) {
		Intent intent = new Intent(this, PaintActivity.class);
		intent.putExtra("artistId", id);
		startActivity(intent);
	}

	@OnClick(R.id.logout)
	public void logout() {
		FirebaseAuth.getInstance().signOut();
		startActivity(new Intent(this, LoginActivity.class));
		finish();
	}
}
